Pod::Spec.new do |s|

  s.name         = "AgoraUnofficalRTMSDK"
  s.version      = "1.0"
  s.summary      = "AgoraUnofficalRTMSDK summary"

 
  s.description  = <<-DESC
                   * AgoraUnofficalRTMSDK - objective C.
                   DESC

  s.homepage     = "https://bitbucket.org/shebinkoshy/agorartmunofficialpod"
  s.license      = { :type => "MIT", :file => "LICENSE" }


  s.author       = { "" => "Shebin Koshy" }

  s.platform     = :ios
  s.platform     = :ios, "9.0"

  s.source       = { :git => "https://shebinkoshy@bitbucket.org/shebinkoshy/agorartmunofficialpod.git", :tag => "1.0" }


 s.vendored_frameworks = "AgoraRtmKit.framework"

end